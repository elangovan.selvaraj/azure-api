module.exports = {
  apps : [{
    script: './bin/www',
    watch: true,
    ignore_watch : ["node_modules", "sample.xlsx","public/uploads"],
    env: {
        "PORT": 3000,
        "NODE_ENV": "development",
        "SECRET_KEY": "DFGDFGS#$%^$%^$546511354651651651651345345dsfgdfgdf"
    },
    env_production: {
        "PORT": 80,
        "NODE_ENV": "production",
        "SECRET_KEY": "DFGDFGS#$%^$%^$546511354651651651651345345dsfgdfgdf"
    }
  }, {
    script: './service-worker/',
    watch: ['./service-worker']
  }],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
