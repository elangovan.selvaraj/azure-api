// var winston = require('winston');

// require('winston-daily-rotate-file');
 
// const logFormat = winston.format.combine(
//     // winston.format.colorize({ all: true }),
//     winston.format.timestamp({ format: 'YYYY/MM/DD HH:mm:ss' }),
//     winston.format.printf(
//         info => { return `${info.timestamp} || ${info.level} || Message: ${info.message}`; },
//     ),
// );
//   var transport = new (winston.transports.DailyRotateFile)({
//     filename: 'logs/application-%DATE%.log',
//     datePattern: 'YYYY-MM-DD-HH',
//     zippedArchive: true,
//     maxSize: '20m',
//     maxFiles: '14d',
//     json: true,
//     handleExceptions: true,
//   }); 

//  var transportCron = new (winston.transports.DailyRotateFile)({
//     filename: 'logs/cron/application-%DATE%.log',
//     datePattern: 'YYYY-MM-DD-HH',
//     zippedArchive: true,
//     maxSize: '20m',
//     maxFiles: '14d',
//     json: true,
//     handleExceptions: true,
//   });
 
//   transport.on('rotate', function(oldFilename, newFilename) {
//     // do something fun
//   });
 
//   var logger = winston.createLogger({
//      level:'info', 
//     format: logFormat,
//     transports: [
//       transport
//     ]
//   }); 

//    var loggerCron = winston.createLogger({
//      level:'info', 
//     format: logFormat,
//     transports: [
//       transportCron
//     ]
//   });
// module.exports = {
//     logger,
//     loggerCron
// }


const winston = require('winston');
process.env.NODE_ENV = "production";
const env = process.env.NODE_ENV;
const fs = require('fs');
var util = require('util');

const logDir = 'logs';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}


const logFormat = winston.format.combine(
    // winston.format.colorize({ all: true }),
    winston.format.timestamp({ format: 'YYYY/MM/DD HH:mm:ss' }),
    winston.format.printf(
        info => { return `${info.timestamp} || ${info.level} || Message: ${info.message}`; },
    ),
);

const successLogger =  winston.createLogger({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      level: "info"
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${logDir}/success/success-%DATE%.log`,
      datePattern: 'YYYY-MM-DD-HH',
      prepend: true,
      level: "info",
      format: logFormat,
      json: true,
      maxSize: '20m',
      maxFiles: '1d',
      zippedArchive: true,
      timestamp : function() {
        return getDateTime();        
      }
    })
  ]
});


const errorLogger =  winston.createLogger({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      level: "error"
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${logDir}/error/error-%DATE%.log`,
      datePattern: 'YYYY-MM-DD-HH',
      prepend: true,
      level: "error",
      format: logFormat,
      json: true,
      maxSize: '20m',
      maxFiles: '1d',
      zippedArchive: true,
      timestamp : function() {
        return getDateTime();        
      }
    })
  ]
});


const cronLogger =  winston.createLogger({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      level: "error"
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${logDir}/cron/cron-%DATE%.log`,
      datePattern: 'YYYY-MM-DD-HH',
      prepend: true,
      level: "info",
      format: logFormat,
      json: true,
      maxSize: '20m',
      maxFiles: '1d',
      zippedArchive: true,
      timestamp : function() {
        return getDateTime();        
      }
    })
  ]
});

if (env === "production") {
  errorLogger.remove(winston.transports.Console);
  successLogger.remove(winston.transports.Console);
}

function getDateTime(){
  var currentdate = new Date(); 
  var datetime = currentdate.getDate() + "/"
    + (currentdate.getMonth()+1)  + "/" 
    + currentdate.getFullYear() + " "  
    + currentdate.getHours() + ":"  
    + currentdate.getMinutes() + ":" 
    + currentdate.getSeconds();
  return datetime;  
}

function formatArgs(args){
  return [util.format.apply(util.format, Array.prototype.slice.call(args))];
}

// console.log = function(){
//   successLogger.info.apply(successLogger, formatArgs(arguments));
// };

// console.error = function(){
//   errorLogger.error.apply(errorLogger, formatArgs(arguments));
// };

module.exports = {
  'successlog': successLogger,
  'errorlog': errorLogger,
  'cronLogger':cronLogger
};