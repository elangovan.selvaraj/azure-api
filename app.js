var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var checkUser = require('./middleware/authMiddleware');


var app = express();

var sockIO = require('socket.io')();
app.sockIO = sockIO;
var sockets1 = require('./config/socket');
sockets1.setSocket(sockIO);
app.use(cookieParser());
app.get('*', checkUser.checkUser);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });
 
app.use(function(req, res, next){
	res.status(404).render('errors/404');
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



// sockIO.on('connection', function(socket) {
//    console.log('A user connected');

//    //Whenever someone disconnects this piece of code executed
//    socket.on('disconnect', function () {
//       console.log('A user disconnected');
//    });
//    socket.emit('testerEvent', { description: 'A custom event named testerEvent!'});
// });


//process.env.	 = 'sv=2019-02-02&st=2021-02-24T11%3A26%3A21Z&se=2022-02-25T11%3A26%3A00Z&sr=c&sp=racwl&sig=leb%2BY0PFq7sPAwoK4VeCI0C2GIxuMTsOSGQglz876cU%3D';



// blobService.createContainerIfNotExists('testcontainerfrommypetra', {
// 	publicAccessLevel: 'blob'
// }, function(error, result, response) {
//   if (!error) {
//   	console.log(error);
//     // if result = true, container was created.
//     // if result = false, container already existed.
//   }
//   console.log(result);
//   console.log(response);
// });


//DELETE

// blobService.deleteContainerIfExists('testcontainerfrommypetra','taskblob2/test', function(error, result, response){
//     if(!error){
//       console.log('container deleted!')
//     }
//     console.log(result);
//     console.log(response);
// });

// blobService.createBlockBlobFromLocalFile('testcontainerfrommypetra', 'taskblob2/test', 'test_csv.csv', function(error, result, response) {
//   if (!error) {
//     // file uploaded
//   }
//   console.log(error);
//   console.log(result);
//   console.log(response);
// });

// http.listen(3000, function () {
//     console.log(`listning on 3000`);
// });
module.exports = app;
