const commonModel       = require("../models/commonModel");

exports.index = (req, res, next) => {
	var result = {
		pageTitle: "Dashboard",
		pageName : "dashboard",
		sessionActive: true,
	}
	res.render('admin/template', result);
}