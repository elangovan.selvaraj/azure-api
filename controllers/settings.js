const commonModel       = require("../models/commonModel");
const errorLog 			= require('../helpers/logging').errorlog;
const successLog 		= require('../helpers/logging').successlog;
const cronLogger 		= require('../helpers/logging').cronLogger;
const bcrypt 			= require('bcrypt');
const format       		= require("../helpers/response");
const path = require('path');

var multer  = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
})
 
var upload = multer({ storage: storage });

exports.get_settings = async (req, res, next) => {
	try{
		var userInfo = siteSettings = {};
		await commonModel.GetTableData('ami_admin_login','',{user_id:1},'')
		.then(data =>{
			let resultArray = Object.values(JSON.parse(JSON.stringify(data.rows)));
	       	userInfo = resultArray[0];
		});

		await commonModel.GetTableData('ami_site_settings','',{id:1},'')
  		.then(async (data) =>   {
  			if(data.rowCount  == 0){
	        }
	        let resultArray = Object.values(JSON.parse(JSON.stringify(data.rows)));
	        siteSettings = resultArray[0];	        
	    });

		var result = {
			pageTitle: "Settings",
			pageName : "settings",
			loadJsFile : "settings",
			sessionActive: true,
			userInfo: userInfo,
			siteSettings : siteSettings
		}
		res.render('admin/template', result);

	}catch(err){
		successLog.error(`Get_settings Error Message : ${err}`);
	}	
}

exports.siteSettingsPost = (req, res, next) => {
	try {
		let { username, password, host, port, storageAccount, sasToken, blobContainer } = req.body;
		var result;
		var updatedData = {
			username : username,
			password : password,
			host : host,
			port : port,
			storageaccountname : storageAccount,
			sastoken : sasToken,
			blobcontainername : blobContainer,
			updated_at : new Date(),
		}
		commonModel.UpdateTableData('ami_site_settings',updatedData,{id:1})
		.then( data =>{
			if(data.rowCount == 1){
				result = format.response(1,'','Settings Updated.');
			}else{
				result = format.response(0,'','Update Failed.');
			}
			res.status(200).json(result);
		});
	}
	catch (err) {
		errorLog.error(`siteSettingsPost Form Error Message : ${err}`);
	}
}

exports.changePasswordPost = async (req, res, next) => {
	try {
		let { currentPassword, newPassword, confirmPassword } = req.body;

		if(newPassword !== confirmPassword){
			let result = format.response(0,'','Password and Confirm Password should be the same');
	        res.status(200).json(result);
		}

		var where = { username:req.cookies.amastUsername };
		commonModel.GetTableData('ami_admin_login','',where,'')
		.then(async data => {
			var resultArray = Object.values(JSON.parse(JSON.stringify(data.rows)));
			const auth = await bcrypt.compare(currentPassword, resultArray[0].password);
			if(!auth){
				let result = format.response(0,'','Please Enter Correct Password');
	        	res.status(200).json(result);
			}
			if(currentPassword === newPassword){
				let result = format.response(0,'','Old Password and Password should not be the same');
	        	res.status(200).json(result);
			}

			const hashPassword = await bcrypt.hashSync(newPassword, 13);

			var updatedData = {
				password : hashPassword,
				updated_at :new Date(),
			}
			commonModel.UpdateTableData('ami_admin_login',updatedData,where)
			.then( data =>{
				if(data.rowCount == 1){
					res.cookie('amastUsername', '', { maxAge: 1 });
					res.cookie('amastPassword', '', { maxAge: 1 });
					res.cookie('amastRememberMe', '', { maxAge: 1 });
					res.cookie('token', '', { maxAge: 1 });
	  				// res.redirect('/');
					let result = format.response(1,'','Password Changed Successfully');
		        	res.status(200).json(result);
		        }else{
		        	let result = format.response(1,'','Failed, try again later');
		        	res.status(200).json(result);
		        }
			})

		})
	}
	catch (err) {
		errorLog.error(`changePasswordPost Form Error Message : ${err}`);
	}
}

exports.userSettingsPost = (req, res, next) => {
	try {
		let upload = multer({ storage: storage }).single('profilePicture');
		upload(req, res, function(err) {
			let { email } = req.body;
	        if (req.fileValidationError) {
	            return res.send(req.fileValidationError);
	        }
	        else if (!req.file) {
	            //return res.send('Please select an image to upload');
	        }
	        else if (err instanceof multer.MulterError) {
	            return res.send(err);
	        }
	        else if (err) {
	            return res.send(err);
	        }

	        var updatedData = {
		    	email:email,
		    	updated_at : new Date(),
		    }

		    if(req.file){
		    	updatedData.profile_picture = req.file.filename;
		    }

		    var where = { username:req.cookies.amastUsername };
		    commonModel.UpdateTableData('ami_admin_login',updatedData,where)
			.then( data =>{
				if(data.rowCount == 1){
					var data = { updatedData };
					let result = format.response(1,data,'Profile Updated Successfully');
		        	res.status(200).json(result);
		        }else{
		        	let result = format.response(1,'','Failed, try again later');
		        	res.status(200).json(result);
		        }
			})
	    });
	}
	catch (err) {
		errorLog.error(`changePasswordPost Form Error Message : ${err}`);
	}
}