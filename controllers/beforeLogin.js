const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const commonModel       = require("../models/commonModel");
const format       = require("../helpers/response");
const errorLog = require('../helpers/logging').errorlog;
const successLog = require('../helpers/logging').successlog;
const cronLogger = require('../helpers/logging').cronLogger;

exports.index = (req, res, next) => {
	var result = {
		pageTitle: "AMAST | Login",
		pageName : "login",
		sessionActive: false,
		loadJsFile : "login",
		amastUsername : req.cookies.amastUsername,
		amastPassword : req.cookies.amastPassword,
		amastRememberMe : req.cookies.amastRememberMe,
	}
	res.render('admin/template', result);
}

const maxAge =  60 * 15; // For 15 mini in secounds
const maxAgeUser = 7 * 24 * 60 * 60; // one week
const createToken = (id) => {
  return jwt.sign({ id }, process.env.SECRET_KEY, {
    expiresIn: maxAge
  });
};

exports.index_post = async (req, res, next) => {
	let { username, password, rememberMe } = req.body;
	try {
  		var where = {
  			username:username
  		}; 
  		commonModel.GetTableData('ami_admin_login','',where,'')
  		.then(async (data) =>   {
  			if(data.rowCount  == 0){
	           let result = format.response(0,'','Invalid Credentials');
	           res.status(200).json(result);
	        }
	        var resultArray = Object.values(JSON.parse(JSON.stringify(data.rows)));
	        const auth = await bcrypt.compare(password, resultArray[0].password);
		    if (auth) {
		      	const token = createToken(resultArray[0].user_id);
				res.cookie('token', token, { httpOnly: true, maxAge: maxAge * 1000  });
				
				if(rememberMe){
					res.cookie('amastUsername', username, { httpOnly: true, maxAge: maxAgeUser * 1000 });
					res.cookie('amastPassword', password, { httpOnly: true, maxAge: maxAgeUser * 1000 });
					res.cookie('amastRememberMe', 'yes', { httpOnly: true, maxAge: maxAgeUser * 1000 });
				}else{	
					res.cookie('amastUsername', '', { maxAge: 1 });
					res.cookie('amastPassword', '', { maxAge: 1 });
					res.cookie('amastRememberMe', '', { maxAge: 1 });
				}
				var redirectUrl = (req.cookies.redirectUrl);
				var data = { redirectUrl };
				let result = format.response(1,data,'Login Succesfully');
				res.cookie('redirectUrl', '', { maxAge: 1 });
				res.status(200).json(result);
		    }else{
			    let result = format.response(0,'','Invalid Credentials');
		        res.status(200).json(result);
		    }
  		})
	}
	catch (err) {
		successLog.error(`Login Post Form Error Message : ${err}`);
	}
}

module.exports.logout_get = (req, res) => {
  res.cookie('token', '', { maxAge: 1 });
  res.redirect('/');
}