const fs                = require("fs");
const Excel             = require('exceljs');
const commonModel       = require("../models/commonModel");
const os                = require('os'); 
const blobService       = require("../config/azureConfig");
const sockets       = require("../config/socket").getSocket();
// console.log('ad',socket);
var base_path = process.env.PWD;
var multiparty = require('multiparty');
var util = require('util');

exports.index = (req, res, next) => {
    commonModel.GetTableData('ami_admin_login','','')
    .then(data => {
        if(data.rowCount  == 0){
            console.log('No Records found');
            return false;
        }
        var rows = data.rows;
        var fields = data.fields;
        let headersCsv = [];
        let headersExcel = [];
        fields.forEach((element) =>{
            var fieldName = element.name;
            headersCsv.push({ header: fieldName, key: fieldName });
            // headersExcel.push(fieldName+'\t');
        });
        var rowsFinalCsv = [];
        var rowsFinalExcel = [];
        console.log(headersExcel);

        // rows.forEach( (element) =>{
        //     var user_id = element.user_id;
        //     var profile_id = element.profile_id;
        //     var role_id = element.role_id;
        //     var distributor_id = element.distributor_id;
        //     var updated_by = element.updated_by;
        //     var created_at = element.created_at;
        //     var updated_at = element.updated_at;

        //     var formResultCsv = user_id+','+profile_id+','+role_id+','+distributor_id+os.EOL;
        //     rowsFinalCsv.push(formResultCsv);

        // })
        // console.log(rowsFinalCsv);
        // var data='user_id\t profile_id\t role_id\t \n';
        // for (var i = 0; i < rows.length; i++) {
        //     data=data+rows[i].user_id+'\t'+rows[i].profile_id+'\t'+rows[i].role_id+'\n';
        //  }

        const workbook = new Excel.Workbook();
        const worksheet = workbook.addWorksheet('ExampleSheet');

        // add column headers
        worksheet.columns = headersCsv;

        rows.forEach( (element) =>{
            worksheet
            .addRow(element)
        })

        workbook
        .xlsx
        .writeFile(base_path+'/sample.xlsx')
        .then((data) => {
            console.log("File Created.");
            blobService.createBlockBlobFromLocalFile('testcontainerfrommypetra', 'taskblob2/Folder.xlsx', base_path+'/sample.xlsx', function(error, result, response) {
                var titleMsg;
                if (!error) {
                // file uploaded
                    if(response.statusCode === 201) {
                        titleMsg = result.name+" Created";
                    }else{
                        if(response.statusCode === 404){
                            titleMsg = "The specified container does not exist.";
                        }else{
                            titleMsg = "Something went wrong";
                        }
                    }
                }else{
                    titleMsg = "Something went wrong";
                }
                console.log(error);
                console.log(result);
                console.log(response);
                res.render('index', { title: titleMsg });
            });

        })
        .catch((err) => {
            console.log("err", err);
        });

        // let writeStream = headersCsv+"\n"+rowsFinal+"\n";
        // fs.writeFile('writeMe.csv', writeStream, function(err, result) {
        //     if(err) console.log('error', err);
        // });

    })
    .catch( err => {
        console.log("error ",err);
    })
};

exports.delete = (req,res,next) =>{
    blobService.deleteContainerIfExists('testcontainerfrommypetra', 'taskblob2',function(error, result, response) {
        var titleMsg;
        if (!error) {
            if(result === true && response.statusCode === 202){ // success //                
                titleMsg = 'Container deleted.';   
            }else{ // error
                if(response.statusCode === 404){
                    titleMsg = 'does not contain or already deleted.';                    
                }else{
                    titleMsg = "Something went wrong";
                }
            }  
        }else{
            titleMsg = "Something went wrong";
        }
        console.log(error);
        console.log(result);
        console.log(response);
        res.render('index', { title: titleMsg });
    });
}

exports.add = (req,res,next) =>{
    blobService.createContainerIfNotExists('testcontainerfrommypetra', {
        publicAccessLevel: 'blob'
    }, function(error, result, response) {
        var titleMsg;
        if (!error) {
            if(response.statusCode === 201){ // success //Created
                titleMsg = result.name+" Created";
            }else{ // overwrite
                if(response.statusCode === 200){
                    titleMsg = result.name+" overwrited";
                }else if(response.statusCode === 409){ // Conflict
                    titleMsg = "When a container is deleted, a container with the same name cannot be created for at least 30 seconds. try again later";
                }else{
                    titleMsg = "Something went wrong";                    
                }
            }
        }else{
            titleMsg = "Something went wrong";
        }
        console.log(error);
        console.log(result);
        console.log(response);
        res.render('index', { title: titleMsg });
    });
}

exports.listContainers = (req,res,next) =>{
    var titleMsg; var containerlist = [];
    blobService.listContainersSegmented(null, function (error, results) {
        if (error) {
            titleMsg = "Something went wrong";
        } else {
            for (var i = 0, container; conainer = results.entries[i]; i++) {
                var containerName = conainer.name;
                containerlist.push(containerName);
            }
            var list = (containerlist.join(','));
            res.render('index', { title: 'container List : '+ list });
        }
    });
}
exports.listBlobs = (req,res,next) =>{
    var titleMsg; var blobslist = [];

    blobService.listBlobsSegmented('testcontainerfrommypetra',null, function (error, results) {
        if (error) {
            titleMsg = "Something went wrong";
        } else {
            for (var i = 0, blob; blob = results.entries[i]; i++) {
                console.log(blob);
                var blobName = blob.name;
                blobslist.push(blobName);
            }
            var list = (blobslist.join(','));
            res.render('index', { title: 'container List : '+ list });
        }
    });
}

exports.uploadFile = (req,res,next) =>{
    res.render('uploadFile', { title: 'uploadFile' });
}
exports.uploadForm = (req,res,next) =>{
   var form = new multiparty.Form();
   form.parse(req);
   form.on('progress', function (bytesReceived, bytesExpected) {
      if (bytesExpected === null) {
        return
      }

      var percentComplete = (bytesReceived / bytesExpected) * 100
      console.log('the form is ' + Math.floor(percentComplete) + '%' + ' complete')
    })
    // form.parse(req, function(err, fields, files) {
        // res.writeHead(200, { 'content-type': 'text/plain' });
        // res.write('received upload:\n\n');
        // res.end(util.inspect({ fields: fields, files: files }));
    // });
   form.on('part', function(part) {
        if(part.filename == ""){
            return;
        }
        if (part.filename) {
            var filename = part.filename;
            var size = part.byteCount;

            // var onError = function(error) {
            //     if (error) {
            //         res.send({ grrr: error });
            //     }
            // };
            // var speedSummary = blobService.createBlockBlobFromBrowserFile('mycontainer', file.name, file, {blockSize : customBlockSize}, function(error, result, response) {
            // finishedOrError = true;
            // if (error) {
            //     // Upload blob failed
            // } else {
            //     // Upload successfully
            // }
        // });

            var customBlockSize = size > 1024 * 1024 * 32 ? 1024 * 1024 * 4 : 1024 * 512;
            blobService.singleBlobPutThresholdInBytes = customBlockSize;

            var speedSummary = blobService.createBlockBlobFromStream('testcontainerfrommypetra', filename, part, size, function(error, result, response){
                    // console.log('error',error);;
                    // console.log('result',result);
                    // console.log('response',response);
            });
            // console.log('speedSummary',speedSummary);
            

                    sockets.on('connection', function(socket) {
                       console.log('A user connected');

                       speedSummary.on('progress', function () {
                    var process = speedSummary.getCompletePercent();
                    console.log('container : '+ process);

                       //Whenever someone disconnects this piece of code executed
                       
                       socket.emit('testerEvent', process +' %');
                       });

                       socket.on('disconnect', function () {
                          console.log('A user disconnected');
                       });
                       // res.write('resonse : '+ process);

                    });


                    res.render('uploadFile', { title: 'container List ' });
                

        } else {
            //form.handlePart(part);
        }
    });
}

exports.download = (req,res,next) =>{
    // var downloadLink = blobService.getUrl('testcontainerfrommypetra', 'taskblob2', 'BlobEndpoint=https://mypetrastorage2.blob.core.windows.net/;QueueEndpoint=https://mypetrastorage2.queue.core.windows.net/;FileEndpoint=https://mypetrastorage2.file.core.windows.net/;TableEndpoint=https://mypetrastorage2.table.core.windows.net/;SharedAccessSignature=sv=2020-02-10&ss=bfqt&srt=sco&sp=rwdlacupx&se=2022-03-31T09:52:44Z&st=2021-03-04T01:52:44Z&spr=https&sig=4zzFtyJ2vEQplC1E0Nph%2FW%2BfUZnYj4Np8fw%2FHPjYaJI%3D');
    // console.log(downloadLink);
    // res.render('index', { title: downloadLink });

    blobService.getBlobToLocalFile('testcontainerfrommypetra', 'testexcel.xls', process.env.PWD, (error, serverBlob) => {
        if (!error) {
          console.log('error',serverBlob);
        }
        console.log('test',serverBlob);
    });
}