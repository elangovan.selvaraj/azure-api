$(function() {    
    $("form[name='changePasswordPost']").validate({
        rules: {
            currentPassword: {
                required: true,
            },
            newPassword: {
                required: true,
                minlength: 5
            },
            confirmPassword: {
                required: true,
                minlength: 5,
                equalTo: "#newPassword"
            },
        },
        messages: {
            currentPassword: {
                required: "Please provide a current password",
            },
            newPassword: {
                required: "Please provide a new password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirmPassword: {
                required: "Please enter a Confirm Password",
                minlength: "Please enter a atleast 5 character",
                equalTo: "Password and Confirm Password should be the same"
            }
        },
        submitHandler: function(form) {
            $('#changePasswordPostBtn').hide();
            $('#changePasswordPostBtn_s').show();
            $.ajax({
                url: '/changePasswordPost',
                method   : 'POST',
                dataType: "json",
                data:$(form).serialize(),
                async: false,
                success:function(data){
                    if(data.status == 0){ //error
                        errorAlert(data.message);
                        $('#changePasswordPostBtn').show();
                        $('#changePasswordPostBtn_s').hide();
                    }else{
                        successAlert(data.message);
                        location.assign('/logout');
                    }
                }
            });
        }
    });

    $("form[name='userSettingsPost']").validate({
        rules: {
            adminUsername: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            username: {
                required: "Please provide a Username",
            },
            email: {
                required: "Please provide a email"
            },
        },
        submitHandler: function(form) {
            $('#userSettingsPostBtn').hide();
            $('#userSettingsPostBtn_s').show();
            $.ajax({
                url: '/userSettingsPost',
                type   : 'POST',
                dataType : "json",
                data :new FormData(form),
                processData: false,
                contentType: false,
                success:function(data){
                    $('#userSettingsPostBtn').show();
                    $('#userSettingsPostBtn_s').hide();
                    if(data.status == 0){ //error
                        errorAlert(data.message);
                    }else{
                        var details = data.data.updatedData;
                        var picture = details && details.profile_picture;
                        console.log('picture',details);
                        if(picture){
                            $("#headerProfilePic").attr('src','/uploads/'+picture);
                        }
                        successAlert(data.message);
                    }
                }
            });
        }
    });

    $("form[name='siteSettings']").validate({
        rules: {
            username:{
                required:true,
            },
            password:{
                required:true,
            },
            host:{
                required:true,
            },
            port:{
                required:true,
            },
            storageAccount:{
                required:true,
            },
            sasToken:{
                required:true,
            },
            blobContainer:{
                required:true,
            }
        },
        messages: {
            
        },
        submitHandler: function(form) {
            $('#siteSettingsBtn').hide();
            $('#siteSettingsBtn_s').show();
            $.ajax({
                url: '/siteSettingsPost',
                type   : 'POST',
                data:$(form).serialize(),
                dataType: "json",
                success:function(data){
                    $('#siteSettingsBtn').show();
                    $('#siteSettingsBtn_s').hide();
                    if(data.status == 0){ //error
                        errorAlert(data.message);
                    }else{
                        successAlert(data.message);
                    }
                }
            });
        }
    });

    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //     find('form').trigger('reset');
    //   });    

});

function handleFiles(files) {
    var imgs = document.querySelectorAll('img');
    //Image element undex
    let imgIndex=0;

    //Loop through all the images
    for(var i = 0; i < files.length; i++){
        var reader = new FileReader();

        reader.onload = function(e) {
          var img = document.getElementById('imgg'+imgIndex);
          img.src = e.target.result; 
          imgIndex++;
        }
        
        reader.readAsDataURL(files[i]);     
   }
}