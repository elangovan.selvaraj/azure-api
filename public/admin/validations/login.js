$(function() {
  $("form[name='loginForm']").validate({
    rules: {
      Username: {
        required: true
      },
      Password: {
        required: true,
        minlength: 5
      }
    },
    messages: {
      Password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      Username: {
        required : "Please enter a username"
      }
    },
    submitHandler: function(form) {
        $('#login_btn').hide();
        $('#login_btn_s').show();
        // form.submit();
        $.ajax({
            url: '/login',
            method   : 'POST',
            dataType: "json",
            data:$(form).serialize(),
            async: false,
            success:function(data){
                if(data.status == 0){ //error
                    errorAlert(data.message);
                    $('#login_btn').show();
                    $('#login_btn_s').hide();
                }else{
                    // setTimeout(function(){
                         successAlert(data.message);
                    // },1000);
                    location.assign(data.data.redirectUrl || '/dashboard');
                }
            }
        });
    }
  });

  $("form[name='forgot_password']").validate({
    rules: {
      Email: {
        required: true,
        email :true,
      }
    },
    messages: {
      Email: {
        required: "Please Enter the Email",
        email: "Please Enter the Valid Email"
      }
    },
    submitHandler: function(form) {
      $('#forgot_btn').hide();
      $('#forgot_btn_s').show();
      form.submit();
    }
  });

   $("form[name='reset_password']").validate({
    rules: {
      Password: {
        required: true,
        minlength: 5
      },
      Conf_Password: {
        required: true,
        minlength: 5,
        equalTo : "#Password"
      },
    },
    messages: {
      Password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      Conf_Password:{
        required : "Please enter a Confirm Password",        
        minlength : "Please enter a atleast 5 character",
        equalTo : "Password and Confirm Password should be the same"
      }
    },
    submitHandler: function(form) {
      $('#reset_btn').hide();
      $('#reset_btn_s').show();
      form.submit();
    }
  });

});