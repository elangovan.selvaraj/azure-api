var base_path = process.env.PWD;

module.exports = {
    BASE_PATH: base_path,
    INVALID_WWW_AUTH :'Sorry - you need valid credentials to be granted access!',
    INVALID_AUTH : 'Invalid Authentication'
};