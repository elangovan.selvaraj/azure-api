const jwt = require('jsonwebtoken');
const commonModel = require("../models/commonModel");
var base_path = process.env.PWD;
module.exports.requireAuth = (req, res, next) => {
    const token = req.cookies.token;
    // check json web token exists & is verified
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, (err, decodedToken) => {
            if (err) {
                res.redirect('/');
            } else {
                next();
            }
        });
    } else {
        var redirectUrl = req.originalUrl;
        res.cookie('redirectUrl', redirectUrl, { httpOnly: true, maxAge: 60 * 15 * 1000 });
        res.redirect('/');
    }
};

// check current user
module.exports.checkUser = (req, res, next) => {
    const token = req.cookies.token;
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, async (err, decodedToken) => {
            if (err) {
                res.locals.user = null;
                next();
            } else {
                var where = {
                    user_id: decodedToken.id
                };
                commonModel.GetTableData('ami_admin_login', '', where, '')
                .then(async (data) => {
                    res.locals.user = data.rows[0];
                    res.locals.baseURL = base_path;
                    next();
                })
            }
        });
    } else {
        res.locals.user = null;
        next();
    }
};

module.exports.loginPageAuth = (req, res, next) => {
    const token = req.cookies.token;
    // check json web token exists & is verified
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, (err, decodedToken) => {
            if (err) {
            }
            res.redirect('/dashboard');
        });
    } else {
        next();
    }
};

// module.exports = { requireAuth, checkUser };