var express = require('express');
var router = express.Router();
var auth = require('../middleware/authMiddleware');


const indexController = require('../controllers/index');
const cronController = require('../controllers/cron');
const beforeLogin 	= require('../controllers/beforeLogin');
const dashboard 	= require('../controllers/dashboard');
const settings 	= require('../controllers/settings');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get("/", auth.loginPageAuth, beforeLogin.index);
router.post("/login", beforeLogin.index_post);
router.get("/dashboard", auth.requireAuth, dashboard.index);
router.get("/logout", beforeLogin.logout_get);
// router.get("/", indexController.index);
router.get("/deleteContainer", indexController.delete);
router.get("/addContainer", indexController.add);
router.get("/listContainers", indexController.listContainers);
router.get("/listBlobs", indexController.listBlobs);
router.get("/uploadFile", indexController.uploadFile);
router.post("/uploadForm", indexController.uploadForm);
router.get("/download", indexController.download);
router.get("/firstCron", cronController.firstCron);
router.get("/secCron", cronController.secCron);
router.get("/stream", cronController.stream);
router.get("/logging", cronController.logging);

router.get('/settings',auth.requireAuth, settings.get_settings);
router.post('/siteSettingsPost',auth.requireAuth, settings.siteSettingsPost);
router.post('/changePasswordPost',auth.requireAuth, settings.changePasswordPost);
router.post('/userSettingsPost',auth.requireAuth, settings.userSettingsPost);


router.post('/uploadFile', function(req, res, next) {
	console.log(res);
  res.render('index', { title: 'Express' });
});

module.exports = router;
