const con = require('../config/dbConfig');
var Utils = require('util');
// utils = require('./utils')

 const InsertTableData =  (table,column) =>{
  return new Promise ((resolve,reject)=>{
  	table = 'integration.'+table;
    con.query("INSERT INTO "+table +" SET ?", column , 
      function(err,result){
          if(err){
            reject(err);
          }else{
            resolve(result.insertId);
          }
      });
  })
}

const  UpdateTableData = (tableName, data = {},conditions = {})  => {
	return new Promise((resolve,reject)=>{
		tableName = 'integration.'+tableName;

		const dKeys = Object.keys(data);
		const dataTuples = dKeys.map((k, index) => `${k} = $${index + 1}`);
		const updates = dataTuples.join(", ");
		const len = Object.keys(data).length;

		let text = `UPDATE ${tableName} SET ${updates} `;

		if (isObject(conditions)) {
			const keys = Object.keys(conditions);
			const condTuples = keys.map((k, index) => `${k} = $${index + 1 + len} `);
			const condPlaceholders = condTuples.join(" AND ");

			text += ` WHERE ${condPlaceholders} RETURNING *`;
		}

		const values = [];
		Object.keys(data).forEach(key => {
			values.push(data[key]);
		});
		Object.keys(conditions).forEach(key => {
			values.push(conditions[key]);
		});
		
		con.query(text,
			values,
		function(err,result){
			if(err){
				reject(err);
			}else{
				resolve(JSON.parse(JSON.stringify(result)))
			}
		})
	})
}

const GetTableData = (table,selectValues='*',where={},orderBy="") => {
	return new Promise((resolve,reject)=>{
		table = 'integration.'+table;
		var where_1,selectValues_1,orderBy_1,whereStatement;

		if(selectValues != ""){
			selectValues_1 = selectValues
		}else{
			selectValues_1 = "*";
		}

		var sqlFormat = "SELECT "+selectValues_1+" FROM "+table;

		if(isObject(where)){
			var whereStatement = "";
			Object.keys(where).forEach(function(key,i) {
				var count = parseInt(i) + parseInt(1);
			  whereStatement+= (key)+' = $'+count.toString()+' AND ';
			});
			whereStatement = whereStatement.slice(0, -4);
			sqlFormat += " WHERE "+whereStatement
		}
		
		if(orderBy != ""){
			orderBy_1 = "ORDER BY "+orderBy
		}else{
			orderBy_1 = "";
		}
		// con.query("SELECT "+selectValues_1+" FROM "+table+" "+where_1+ " "+orderBy_1,
		con.query(sqlFormat,
			Object.values(where),
		function(err,result){
			if(err){
				reject(err);
			}else{
				resolve(JSON.parse(JSON.stringify(result)))
			}
		})
	})
}

const CustomQuery = ($query) => {
	return new Promise((resolve,reject)=>{
		con.query($query,
		function(err,result){
			if(err){
				reject(err);
			}else{
				resolve(result)
			}
		})
	})
}

module.exports.InsertTableData = InsertTableData;
module.exports.UpdateTableData = UpdateTableData;
module.exports.GetTableData    = GetTableData;
module.exports.CustomQuery     = CustomQuery;

var isObject = function(a) {
    return (!!a) && (a.constructor === Object);
};